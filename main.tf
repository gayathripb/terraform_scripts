provider "aws" {
  region = "us-east-1"
}

# terraform apply -var "server_port=8080"
# export TF_VAR_server_port

variable "server_port" {
  type = number
}

variable "num_servers" {
  type = number
}

resource "aws_instance" "example" {
  count = var.num_servers
  ami = "ami-00eb20669e0990cb4"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance.id]
  user_data = <<-EOF
              #!/bin/bash
              sudo yum install httpd -y
              sudo service httpd start
              cd /var/www/html/
              mkdir sample
              sudo chown ec2-user /var/www/html/sample
              sudo chmod -R o+r sample
              cd /var/www/html/sample/
              echo "<html><h1>Hello!! My first TERRAFORM SCRIPT </h1></html>" > index.html
              EOF
  key_name = "sample"
  tags = {
    Name = "terraform-examples - ${count.index}"
  }
}

resource "aws_security_group" "instance" {
 name = "terraform-example-instance"
  ingress {
    from_port = var.server_port
    to_port = var.server_port
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}